export async function authenticate() {
    try {
        await gapi.auth2.getAuthInstance()
            .signIn({scope: "https://www.googleapis.com/auth/youtube.readonly"});
        console.log("Sign-in successful");
    } catch (err) {
        console.error("Error signing in", err);
    }
}

export async function loadClient() {
    gapi.client.setApiKey("AIzaSyDIITzUNLgWkVPMzX4gFS7p6hXVwRIOuyM");
    try {
        await gapi.client.load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest");
        console.log("GAPI client loaded for API");
    } catch (err) {
        console.error("Error loading GAPI client for API", err);
    }
}

export type VideoSnippet = {
    publishedAt:string;
    channelId:string;
    title:string;
    description:string;
    thumbnails:{
        default:Thumbnail;
        medium:Thumbnail;
        high:Thumbnail;
        standard:Thumbnail;
        maxres:Thumbnail;
    },
    channelTitle:string;
    tags:string[];
    categoryId:string;
    liveBroadcastContent:string;
    defaultAudioLanguage:string;
    localized:{
        title:string;
        description:string;
    };
    defaultLanguage:string;

};
export type Thumbnail = {
    url:string;
    width:number;
    height:number;
}
export async function getVideoSnippets(ids:string[]):Promise<{items:{kind:string,id:string,snippet:VideoSnippet}[]}> {
    try {

        const response = await gapi.client.youtube.videos.list({
            "part": [
                "snippet"
            ],
            "id": ids
        });
        console.log("Response", response);
        return response.result;
    } catch (err) {
        console.error("Execute error", err);
        return [];
    }
}
