export type Rectangle = {
    x: number;
    y: number;
    width: number;
    height: number;
    stroke?: string;
    'stroke-width'?: number;
    fill?: string;
    id: string;
}
export type Path = {
    d: string;
    stroke: string;
    'stroke-width': number;
    fill: string;
    id:string;
}
const curveRadius = 10;

export function getPath(rect1: Rectangle, rect2: Rectangle, verticalOffset = 0, horizontalOffset = 0,terminalHorizontalOffset=0): Path {
    const x1 = rect1.x + rect1.width;
    const y1 = rect1.y + rect1.height / 2 + verticalOffset;
    const x2 = rect2.x;
    const y2 = rect2.y + rect2.height / 2 + terminalHorizontalOffset;
    let x3 = x2 - x1 - curveRadius * 2;
    let y3 = y2 - y1 - curveRadius * 2;
    if(y1-verticalOffset===y2){
        const d = `
        M ${x1} ${y1} 
        L ${x2} ${y1}
        `
        return {d, stroke: 'black', 'stroke-width': 4, fill: 'none', id: `${rect1.id}-${rect2.id}`}
    }
    const verticalDirection = y3 / Math.abs(y3);
    const horizontalDirection = x3 / Math.abs(x3);
    if (verticalDirection === -1)
        y3 = y3 + curveRadius * 4;
    if(horizontalDirection===-1)
        x3 = x3 + curveRadius * 4;
    const d = `M ${x1} ${y1} h ${10 + horizontalOffset}
        c 0,0 ${curveRadius},0 ${curveRadius},${curveRadius * verticalDirection}
        v ${y3}
        c 0,0 0,${curveRadius * verticalDirection} ${curveRadius*horizontalDirection},${curveRadius * verticalDirection}
        h ${x3}`;
    return {d, stroke: 'black', 'stroke-width': 4, fill: 'none', id: `${rect1.id}-${rect2.id}`}
}

export function getPadding(rectangle: Rectangle, offset = 0): Path {
    const curveRadius = 5;
    const d = `
        M ${rectangle.x - offset} ${rectangle.y}
        v ${rectangle.height - (curveRadius - offset)}
        c 0,0 0,${curveRadius} ${curveRadius},${curveRadius}
        h ${rectangle.width + (offset - curveRadius) * 2}
        c 0,0 ${curveRadius},0 ${curveRadius},${-curveRadius}
        v ${-rectangle.height - (offset - curveRadius) * 2}
        c 0,0 0,${-curveRadius} ${-curveRadius},${-curveRadius}
        h ${-rectangle.width - (offset - curveRadius) * 2}
        c 0,0 ${-curveRadius},0 ${-curveRadius},${curveRadius}
        v ${offset}
        `;
    return {d, stroke: '#bbb', 'stroke-width': 2, fill: 'white', id: `${rectangle.id}-padding`}
}

