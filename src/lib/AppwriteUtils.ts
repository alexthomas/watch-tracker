import {Account, Client, Databases} from "appwrite";

export const ENDPOINT='https://appwrite.alexk8s.com/v1';
export const PROJECT = '64ce937e86618682be20'
export const client = new Client()
    .setEndpoint(ENDPOINT)
    .setProject(PROJECT);
export const accountClient = new Account(client);
export const databaseClient = new Databases(client);

export const DATABASE = {
    id: "64ce941adccbbe08af39",
    tables: {
        VIDEOS: "64ce941ff331c7868650",
        ROOT: '64ce9430817b5c9d7d75'
    }
}
