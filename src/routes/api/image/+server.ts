import {json} from "@sveltejs/kit";

export async function GET({url}){

    // return new Response(json)
    const imageUrl = url.searchParams.get("imageUrl");
    const response = await fetch(imageUrl);
    const imageBlob = await response.blob();
    const arrayBuffer = await imageBlob.arrayBuffer();
    return new Response(arrayBuffer, {headers: {"Content-Type": response.headers.get("Content-Type")+""}})
}